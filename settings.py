from pydantic import BaseSettings, Field
from typing import Dict, List

class InfluxDBSettings(BaseSettings):
    host :str = Field(..., env='INFLUXDB_HOST')
    port : int = Field(..., env='INFLUXDB_PORT')
    username : str = Field(..., env='INFLUXDB_USERNAME')
    password : str = Field(..., env='INFLUXDB_PASSWORD')
    database : str = Field(..., env='INFLUXDB_DATABASE')
    ssl : bool = Field(True, env='INFLUXDB_SSL')
    verify_ssl : bool = Field(False, env='INFLUXDB_VERIFYSSL')
    measurement : str = "raspberry"
    tags : Dict[str, str] = {}  
    
    class Config:
        env_prefix = 'INFLUXDB_'
        env_file = '.env'
    
    
    