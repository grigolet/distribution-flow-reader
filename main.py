#!/usr/bin/python

######################################################################
#
# IF YOU GET IO-READ ERROR - YOU ARE MISSING THE SUCCESSIVE READ
# CAPABILITY ENABLED ON THE PI.
# For I2C and SMBus Documentation see
# http://wiki.erazor-zone.de/wiki:linux:python:smbus:doc
#
# See repeated start discussion or successive read information at
#    https://www.raspberrypi.org/forums/viewtopic.php?f=44&t=15840&start=25
#
# sudo su -
# echo -n 1 > /sys/module/i2c_bcm2708/parameters/combined
# exit
# sudo reboot
# File /sys/module/i2cbcm2708/parameters/combined changed from N to Y
#
# Add line
# echo -n 1 > /sys/module/i2c_bcm2708/parameters/combined
# in /etc/rc.local before the last line. After adding the line, reboot
# the pi.
# See Pi-16ADC User Guide.
#
######################################################################
# (C) ALCHEMY POWER INC 2016,2017 etc. - ALL RIGHT RESERVED.
# CODE SUBJECT TO CHANGE AT ANY TIME.
# YOU CAN COPY/DISTRIBUTE THE CODE AS NEEDED AS LONG AS YOU MAINTAIN
# THE HEADERS (THIS PORTION) OF THE TEXT IN THE FILE.
######################################################################

import time
import smbus
import sys
import os
import subprocess
from scipy.interpolate import interp1d
from settings import InfluxDBSettings
from influxdb import InfluxDBClient
import urllib3
from smbus import SMBus
from sys import exit

urllib3.disable_warnings()

settings = InfluxDBSettings(tags={"setup": "manifold", "place": "ATLAS", "device": "epdt-rasp1"})
print("Modules imported")
print("Parsed settings from .env file: ")
print(settings)
influxdb_client = InfluxDBClient(settings.host, settings.port, settings.username, 
                        settings.password, settings.database, ssl=settings.ssl, 
                        verify_ssl=settings.verify_ssl)



# for older PI's (version 1) use bus = SMBus(0) in statement below.
bus = SMBus(1)

adc_address =  0b1110110 #  HIGH  HIGH   HIGH    0x76
channels = {
"1": 0xB0,
"2": 0xB8,
"3": 0xB1,
"4": 0xB9,
"5": 0xB2,
"6": 0xBA,
"7": 0xB3,
"8": 0xBB,
"9": 0xB4,
"10": 0xBC,
"11": 0xB5,
"12": 0xBD,
"13": 0xB6,
"14": 0xBE,
"15": 0xB7,
"16": 0xBF,
}
calibration_curve = interp1d([0, 0.1, 0.22, 0.5, 1.6, 2.1, 2.31, 2.5], [-3.7, -3, -2, 0, 0.25*60, 0.5*60, 0.75*60, 1*60], kind="cubic")
vref = 5
max_reading = 2**23

# Now we determine the operating parameters.
# lange = number of bytes to read. A minimum of 3 bytes are read in. In this sample we read in 6 bytes,
# ignoring the last 3 bytes.
# zeit (German for time) - tells how frequently you want the readings to be read from the ADC. Define the
# time to sleep between the readings.
# tiempo (Spanish - noun - for time) shows how frequently each channel is read in over the I2C bus. Best to use
# timepo between each successive readings.

lange = 0x06 # number of bytes to read in the block
zeit = 0     # number of seconds to sleep between each measurement
tiempo = 0.25 # number of seconds to sleep between each channel reading

# tiempo - has to be more than 0.2 (seconds).
def getreading(adc_address, adc_channel):
    bus.write_byte(adc_address, adc_channel)
    time.sleep(tiempo)
    reading  = bus.read_i2c_block_data(adc_address, adc_channel, lange)
    adc_value = ((((reading[0]&0x3F))<<16))+((reading[1]<<8))+(((reading[2]&0xE0)))
    volts = adc_value*vref/max_reading

    if( (reading[0]& 0b11000000) == 0b11000000): # we found the error
        pass
    return volts

time.sleep(tiempo)

while (True):
    os.system("clear")
    for channel_num, channel_address in channels.items():
        fields = {}
        value = getreading(adc_address, channel_address)
        flow = calibration_curve(value)
        fields["voltage"] = value
        fields["flow"] = flow
        tags = settings.tags
        tags["channel"] = channel_num 
        # Save data to db
        influxdb_client.write_points([{
            "measurement": settings.measurement,
            "tags": tags,
            "fields": fields
        }])
        print (f"Channel {channel_num:>5} = {value:>5.2f} V Flow: {flow:>5.2f}")
        # Sleep between each reading.
        time.sleep(tiempo)
    
    print("Data written to db")
    time.sleep(zeit)
# End of main loop.

